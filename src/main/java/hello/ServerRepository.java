package hello;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ServerRepository extends MongoRepository<Server, String> {

    public Server findByHostname(String hostname);
    public List<Server> findByUserSid(String userSid);

}
