package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class Application implements CommandLineRunner {


    @Autowired
    private ServerRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
		repository.deleteAll();
		// save a couple of servers
		repository.save(new Server("cblc0001", "catty", new Date(), new Date()));
		repository.save(new Server("cblc0002", "12345", new Date(), new Date()));
    }

}
