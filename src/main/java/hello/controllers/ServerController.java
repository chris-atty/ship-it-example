package hello.controllers;

import hello.Server;
import hello.ServerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ServerController {

    @Autowired
    private ServerRepository repository;

    @RequestMapping("/view")
    public String view(Model model) {
        List<Server> servers = repository.findAll();
        model.addAttribute("servers", servers);
        return "view";
    }

    @RequestMapping(path = "/add", method= RequestMethod.POST)
    public String add(Model model) {
        return "form";
    }

}
