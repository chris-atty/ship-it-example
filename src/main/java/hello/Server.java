package hello;

import org.springframework.data.annotation.Id;

import java.util.Date;


public class Server {

    @Id
    private String id;

    private String hostname;
    private String userSid;
    private Date creationDate;
    private Date expiryDate;

    public Server(String hostname, String userSid, Date creationDate, Date expiryDate) {
        this.hostname = hostname;
        this.userSid = userSid;
        this.creationDate = creationDate;
        this.expiryDate = expiryDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getUserSid() {
        return userSid;
    }

    public void setUserSid(String userSid) {
        this.userSid = userSid;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return hostname;
    }

}
